from functools import reduce


def find(pred, items: list):
    return next(filter(pred, items), None)


def find_id(pred, items: list):
    return next(
        filter(
            lambda item: pred(item[1]),
            enumerate(items)
        ),
        (None, None)
    )[0]


def count_item_in_list(value, items: list):
    return reduce(
        lambda acc, item: acc + 1 if value == items else acc,
        items,
        0
    )


def flat(lists: list):
    return [
        item
        for sublist in lists
        for item in sublist
    ]


def make_pairs(items):
    pairs = []
    for id, item in enumerate(items):
        if id + 1 < len(items):
            pairs.append((item, items[id + 1]))
        else:
            pairs.append((item, items[0]))
    return pairs
