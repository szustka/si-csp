from src.csp_solver import CSPSolver

if __name__ == "__main__":
    domains = {
        "A": [0, 1, 2, 3],
        "B": [1, 2, 3, 4],
        "C": [1, 2, 3]
    }
    problem = CSPSolver(
        list(domains.keys()),
        domains
    )

    problem.add_constraint(
        ["A", "B"],
        lambda assignments:
            "A" not in assignments or
            "B" not in assignments or
            assignments["A"] > assignments["B"]
    )
    problem.add_constraint(
        ["B", "C"],
        lambda assignments:
            "B" not in assignments or
            "C" not in assignments or
            assignments["B"] > assignments["C"])

    print("--- Domains ---")
    print("Domain:", problem.domains)
    print("Results:", problem.forward_checking_search_all())

    problem.run_ac3()

    print("--- After AC3 ---")
    print("Domain:", problem.domains)
    print("Results:", problem.forward_checking_search_all())
