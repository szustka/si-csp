import toolz.curried as tzc
import toolz as tz
import copy

from src.helpers import find


class CSPSolver:
    __labels: list = None
    __domains: dict = None
    __constraints: dict = None
    __connected_labels: dict = None
    __ac3_arcs: list = None

    __select_label_strategy = None
    __sort_domain_strategy = None

    def __init__(self, labels: list, domains: dict):
        self.__labels = labels
        self.__domains = domains

        self.__ac3_arcs = []
        self.__constraints = {}
        self.__connected_labels = {}
        for label in labels:
            self.__constraints[label] = []
            self.__connected_labels[label] = []

    def backtracking_search(self, assignment=None):
        if assignment is None:
            assignment = {}
        if len(assignment) == len(self.__labels):
            return assignment

        unassigned_labels = [
            label
            for label in self.__labels
            if label not in assignment
        ]
        first_label = unassigned_labels[0]

        for value in self.__domains[first_label]:
            local_assignment = assignment.copy()
            local_assignment[first_label] = value

            if self.is_consistent(first_label, local_assignment):
                result = self.backtracking_search(local_assignment)
                if result is not None:
                    return result
        return None

    def backtracking_search_all(self, assignment=None):
        if assignment is None:
            assignment = {}
        if len(assignment) == len(self.__labels):
            return [assignment]

        unassigned_labels = [
            label
            for label in self.__labels
            if label not in assignment
        ]
        first_label = unassigned_labels[0]

        working_assignments = []
        for value in self.__domains[first_label]:
            local_assignment = assignment.copy()
            local_assignment[first_label] = value

            if self.is_consistent(first_label, local_assignment):
                working_assignments.extend(
                    self.backtracking_search_all(local_assignment)
                )
        return working_assignments

    def forward_checking_search(self, assignment=None, domains_mask=None):
        if assignment is None:
            assignment = {}
        if domains_mask is None:
            domains_mask = {
                label: [0] * len(domain)
                for label, domain in self.__domains.items()
            }
        if len(assignment) == len(self.__labels):
            return assignment

        unassigned_labels = [
            label
            for label in self.__labels
            if label not in assignment
        ]
        updated_domains_mask = self.__update_mask(
            domains_mask,
            unassigned_labels,
            assignment
        )
        chosen_label = self.__select_label(
            unassigned_labels,
            updated_domains_mask
        )
        sorted_domain = self.__sort_domain(
            chosen_label,
            self.__domains,
            domains_mask,
            unassigned_labels,
            assignment
        )
        for value_id, value in enumerate(sorted_domain):
            if updated_domains_mask[chosen_label][value_id] == 0:
                solution = self.forward_checking_search(
                    {**assignment, chosen_label: value},
                    updated_domains_mask
                )
                if solution:
                    return solution
        return None

    def forward_checking_search_all(self, assignment=None, domains_mask=None):
        if assignment is None:
            assignment = {}
        if domains_mask is None:
            domains_mask = {
                label: [0] * len(domain)
                for label, domain in self.__domains.items()
            }
        if len(assignment) == len(self.__labels):
            return [assignment]

        unassigned_labels = [
            label
            for label in self.__labels
            if label not in assignment
        ]
        updated_domains_mask = self.__update_mask(
            domains_mask,
            unassigned_labels,
            assignment
        )
        chosen_label = self.__select_label(
            unassigned_labels,
            updated_domains_mask
        )
        sorted_domain = self.__sort_domain(
            chosen_label,
            self.__domains,
            domains_mask,
            unassigned_labels,
            assignment
        )

        working_assignments = []
        for value_id, value in enumerate(sorted_domain):
            if updated_domains_mask[chosen_label][value_id] == 0:
                solutions = self.forward_checking_search_all(
                    {**assignment, chosen_label: value},
                    updated_domains_mask
                )
                working_assignments.extend(solutions)
        return working_assignments

    def __update_mask(self, domains_mask: dict, unassigned_labels: list, assignment: dict):
        domain_mask_copy = copy.deepcopy(domains_mask)
        for cur_label in unassigned_labels:
            for mask_id, mask_value in enumerate(domain_mask_copy[cur_label]):
                if mask_value == 0:
                    temp_assignment = {
                        **assignment,
                        cur_label: self.__domains[cur_label][mask_id]
                    }
                    if not self.is_consistent(cur_label, temp_assignment):
                        domain_mask_copy[cur_label][mask_id] = 1
        return domain_mask_copy

    def __select_label(self, unassigned_labels: list, domains_mask: dict = None):
        if self.__select_label_strategy:
            return self.__select_label_strategy(
                unassigned_labels=unassigned_labels,
                connected_labels=self.__connected_labels,
                domains=self.__domains,
                domains_mask=domains_mask
            )
        return unassigned_labels[0]

    def __sort_domain(self, label, domains: dict, domains_mask: dict, unassigned_labels: list, assignment: dict):
        if self.__sort_domain_strategy:
            return self.__sort_domain_strategy(
                label=label,
                unassigned_labels=unassigned_labels,
                domains=domains,
                domains_mask=domains_mask,
                assignment=assignment,
                update_mask=self.__update_mask,
            )
        return domains[label]

    def run_ac3(self):
        queue = list(self.__ac3_arcs)
        while len(queue):
            (first_label, second_label, constraint) = queue.pop(0)

            modified_domain = list(self.__domains[first_label])
            for first_label_value in self.__domains[first_label]:
                valid_constraints = 0
                for second_label_value in self.__domains[second_label]:
                    assignment = {
                        first_label: first_label_value,
                        second_label: second_label_value
                    }
                    if constraint(assignment):
                        valid_constraints += 1

                if valid_constraints == 0:
                    modified_domain.remove(first_label_value)
                    arcs_with_first = list(filter(
                        lambda arc: arc[1] == first_label and arc not in queue,
                        self.__ac3_arcs
                    ))
                    queue.extend(arcs_with_first)
            self.__domains[first_label] = modified_domain

    def add_constraint(self, labels: list, constraint):
        for label in labels:
            self.__constraints[label].append(constraint)
            connected_labels = [
                cur_label
                for cur_label in labels
                if cur_label != label
            ]
            self.__connected_labels[label].extend(connected_labels)

        if len(labels) == 2:
            self.__ac3_arcs.append((labels[0], labels[1], constraint))
            self.__ac3_arcs.append((labels[1], labels[0], constraint))

    def is_consistent(self, label, label_assignments: dict):
        for constraint in self.__constraints[label]:
            if not constraint(label_assignments):
                return False
        return True

    def set_select_label_strategy(self, select_label_strategy):
        self.__select_label_strategy = select_label_strategy

    def set_sort_values_strategy(self, sort_values_strategy):
        self.__sort_domain_strategy = sort_values_strategy

    @property
    def domains(self):
        return self.__domains
