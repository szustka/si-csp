from src.csp_solver import CSPSolver
from src.helpers import make_pairs

if __name__ == "__main__":
    men = ["norwegian", "englishman", "dane", "german", "swede"]
    pets = ["fishes", "cats", "birds", "dogs", "horses"]
    tobaccos = ["light", "cigar", "pipe", "unfiltered", "menthol"]
    colors = ["red", "green", "white", "yellow", "blue"]
    drinks = ["tea", "milk", "water", "beer", "coffee"]

    man_domains = {
        (1, "man"): men,
        (2, "man"): men,
        (3, "man"): men,
        (4, "man"): men,
        (5, "man"): men
    }
    pet_domains = {
        (1, "pet"): pets,
        (2, "pet"): pets,
        (3, "pet"): pets,
        (4, "pet"): pets,
        (5, "pet"): pets
    }
    tobacco_domains = {
        (1, "tobacco"): tobaccos,
        (2, "tobacco"): tobaccos,
        (3, "tobacco"): tobaccos,
        (4, "tobacco"): tobaccos,
        (5, "tobacco"): tobaccos
    }
    color_domains = {
        (1, "color"): colors,
        (2, "color"): colors,
        (3, "color"): colors,
        (4, "color"): colors,
        (5, "color"): colors,
    }
    drink_domains = {
        (1, "drink"): drinks,
        (2, "drink"): drinks,
        (3, "drink"): drinks,
        (4, "drink"): drinks,
        (5, "drink"): drinks,
    }

    domains = {}
    domains.update(man_domains)
    domains.update(pet_domains)
    domains.update(tobacco_domains)
    domains.update(color_domains)
    domains.update(drink_domains)

    problem = CSPSolver(
        list(domains.keys()),
        domains
    )

    def double_constraints_generator(csp: CSPSolver, first, second, constraint):
        (first_prop, first_value) = first
        (second_prop, second_value) = second

        def constraint_helper(assignments):
            first_house = None
            second_house = None
            for (house, prop), value in assignments.items():
                if prop == first_prop and value == first_value:
                    first_house = house
                if prop == second_prop and value == second_value:
                    second_house = house
            return not first_house or not second_house or constraint(first_house, second_house)

        for first_id in range(1, 5):
            for second_id in range(1, 5):
                first_key = (first_id, first_prop)
                second_key = (second_id, second_prop)
                csp.add_constraint([first_key, second_key], constraint_helper)

    def equal(first, second):
        return first == second

    def adjacent(first, second):
        return first + 1 == second or first - 1 == second

    def every_value_different_constraints_generator(csp: CSPSolver):
        for pair in make_pairs(list(domains.keys())):
            csp.add_constraint(
                [pair[0], pair[1]],
                lambda assignments:
                    pair[0] not in assignments or
                    pair[1] not in assignments or
                    assignments[pair[0]] != assignments[pair[1]]
            )
    every_value_different_constraints_generator(problem)

    # 1. Norweg zamieszkuje pierwszy dom
    problem.add_constraint(
        [(1, "man"), (1, "man")],
        lambda assignments: assignments[1, "man"] == "norwegian"
    )

    # 2. Anglik mieszka w czerwonym domu
    double_constraints_generator(
        problem,
        ("man", "englishman"),
        ("color", "red"),
        equal
    )

    # 3. Zielony dom znajduje się bezpośrednio po lewej stronie domu białego.
    double_constraints_generator(
        problem,
        ("color", "green"),
        ("color", "white"),
        lambda green_house, white_house: green_house + 1 == white_house
    )

    # 4. Duńczyk pija herbatkę
    double_constraints_generator(
        problem,
        ("man", "dane"),
        ("drink", "tea"),
        equal
    )

    # 5. Palacz papierosów light mieszka obok hodowcy kotów.
    double_constraints_generator(
        problem,
        ("tobacco", "light"),
        ("pet", "cats"),
        adjacent
    )

    # 6. Mieszkaniec żółtego domu pali cygara.
    double_constraints_generator(
        problem,
        ("color", "yellow"),
        ("tobacco", "cigar"),
        equal
    )

    # 7. Niemiec pali fajkę.
    double_constraints_generator(
        problem,
        ("man", "german"),
        ("tobacco", "pipe"),
        equal
    )

    # 8. Mieszkaniec środkowego domu pija mleko.
    problem.add_constraint(
        [(3, "drink"), (3, "drink")],
        lambda assignments: assignments[(3, "drink")] == "milk"
    )

    # 9. Palacz papierosów light ma sąsiada, który pija wodę.
    double_constraints_generator(
        problem,
        ("tobacco", "light"),
        ("drink", "water"),
        adjacent
    )

    # 10. Palacz papierosów bez filtra hoduje ptaki.
    double_constraints_generator(
        problem,
        ("tobacco", "unfiltered"),
        ("pet", "birds"),
        equal
    )

    # 11. Szwed hoduje psy
    double_constraints_generator(
        problem,
        ("man", "swede"),
        ("pet", "dogs"),
        equal
    )

    # 12. Norweg mieszka obok niebieskiego domu.
    double_constraints_generator(
        problem,
        ("man", "norwegian"),
        ("color", "blue"),
        adjacent
    )

    # 13. Hodowca koni mieszka obok żółtego domu.
    double_constraints_generator(
        problem,
        ("pet", "horses"),
        ("color", "yellow"),
        adjacent
    )

    # 14. Palacz mentolowych pija piwo.
    double_constraints_generator(
        problem,
        ("tobacco", "menthol"),
        ("drink", "beer"),
        equal
    )

    # 15. W zielonym domu pija się kawę.
    double_constraints_generator(
        problem,
        ("color", "green"),
        ("drink", "coffee"),
        equal
    )

    print("--- Domains ---")
    print("Domain:", problem.domains)

    problem.run_ac3()

    print("--- After AC3 ---")
    print("Domain:", problem.domains)

    # print(problem.forward_checking_search())

    # for line in problem.forward_checking_search_all():
    #     print(line)
