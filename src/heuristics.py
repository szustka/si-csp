from src.helpers import count_item_in_list, flat
import toolz as tz


def select_label_most_constraining(unassigned_labels, connected_labels, domains, domains_mask):
    return sorted(
        unassigned_labels,
        key=lambda label: len(connected_labels[label])
    )[0]


def select_label_least_constraining(unassigned_labels, connected_labels, domains, domains_mask):
    return sorted(
        unassigned_labels,
        key=lambda label: len(connected_labels[label]),
        reverse=True
    )[0]


def select_label_most_constrained(unassigned_labels, connected_labels, domains, domains_mask):
    return sorted(
        unassigned_labels,
        key=lambda label: count_item_in_list(0, domains_mask[label])
    )[0]


def select_label_least_constrained(unassigned_labels, connected_labels, domains, domains_mask):
    return sorted(
        unassigned_labels,
        key=lambda label: count_item_in_list(0, domains_mask[label]),
        reverse=True
    )[0]


def sort_domain_most_constraining(label, unassigned_labels, domains, domains_mask, assignment, update_mask):
    return sorted(
        domains[label],
        key=lambda value: tz.pipe(
            update_mask(domains_mask, unassigned_labels, {
                **assignment,
                label: value
            }).values(),
            flat,
            tz.curry(count_item_in_list)(0)
        )
    )


def sort_domain_least_constraining(label, unassigned_labels, domains, domains_mask, assignment, update_mask):
    return sorted(
        domains[label],
        key=lambda value: tz.pipe(
            update_mask(domains_mask, unassigned_labels, {
                **assignment,
                label: value
            }).values(),
            flat,
            tz.curry(count_item_in_list)(0)
        ),
        reverse=True
    )
