import dataclasses


@dataclasses.dataclass
class Constraint:
    labels: list

    def satisfied(self, label_assignments: dict):
        return False


class DoubleConstraint(Constraint):
    first_prop

    def __init__(self, first, second):
        (first_prop, first_value) = first
        (second_prop, second_value) = second

