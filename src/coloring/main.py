from src.coloring.helpers import generate_connections
from src.csp_solver import CSPSolver
import toolz.curried as tzc
import src.heuristics as hc
import toolz as tz
import random

if __name__ == "__main__":
    size = 5
    points_count = 20
    colors_count = 2

    points = tz.pipe(
        range(points_count),
        tzc.map(lambda _: (
            random.randrange(size),
            random.randrange(size)
        )),
        set,
        list
    )
    paths_data = generate_connections(size, points)

    domains = {}
    for point in points:
        domains[point] = list(range(colors_count))

    problem = CSPSolver(points, domains)

    def constraint_builder(start_point: tuple, end_point: tuple):
        def constraint(assignment: dict):
            if start_point not in assignment or end_point not in assignment:
                return True
            return assignment[start_point] != assignment[end_point]
        return constraint

    for path_data in paths_data:
        (start, end) = path_data
        problem.add_constraint(
            [start, end],
            constraint_builder(start, end)
        )

    problem.set_select_label_strategy(hc.select_label_least_constrained)
    problem.set_sort_values_strategy(hc.sort_domain_least_constraining)

    # print("--- normal ---")
    # for line in problem.domains.items():
    #     print(line)
    #
    # problem.run_ac3()
    #
    # print("--- after ac3 ---")
    # for line in problem.domains.items():
    #     print(line)

    print(problem.forward_checking_search())
    # for line in problem.forward_checking_search_all():
    #     print(line)
