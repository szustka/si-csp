from src.coloring.types import Segment, Direction
from src.coloring.types import Point
import numpy as np
import random


def connect_points_by_path(start_point: tuple, end_point: tuple):
    x_delta = start_point[0] - end_point[0]
    x_direction = Direction.RIGHT if x_delta < 0 else Direction.LEFT
    x_segment = Segment(direction=x_direction, length=abs(x_delta))

    y_delta = start_point[1] - end_point[1]
    y_direction = Direction.DOWN if y_delta < 0 else Direction.UP
    y_segment = Segment(direction=y_direction, length=abs(y_delta))

    if x_delta == 0:
        return [y_segment]
    if y_delta == 0:
        return [x_segment]

    return [x_segment, y_segment]


def count_length_between_points(start_point: tuple, end_point: tuple):
    path = connect_points_by_path(start_point, end_point)
    length = 0
    for segment in path:
        length += segment.length
    return length


def has_intersections(size: int, paths_data: list, paths: list):
    matrix = np.zeros((size, size))

    for index, segments in enumerate(paths):
        path_id = index + 1
        path_data = paths_data[index]
        pointer = Point(x=path_data[0][0], y=path_data[0][1])

        for segment in segments:
            for _ in range(segment.length):
                matrix[pointer.x][pointer.y] = path_id
                pointer.move(segment.direction)

                if matrix[pointer.x][pointer.y] != 0:
                    return True

    return False


def generate_connections(size: int, points: list, iterations=500):
    points_connections = {}
    for point in points:
        points_connections[point] = []

    paths_data = []
    paths = []
    for _ in range(iterations):
        start_point = random.choice(points)
        unconnected_points = [
            point
            for point in points
            if point not in points_connections[start_point] and point is not start_point
        ]
        closest_points = sorted(
            unconnected_points,
            key=lambda point: count_length_between_points(start_point, point)
        )
        if closest_points:
            temp_paths_data = paths_data + [(start_point, closest_points[0])]
            temp_paths = paths + [connect_points_by_path(start_point, closest_points[0])]

            if not has_intersections(size, temp_paths_data, temp_paths):
                paths_data = temp_paths_data
                paths = temp_paths
                points_connections[start_point].append(closest_points[0])

    # for line in points_connections.items():
    #     print(line)

    return paths_data
